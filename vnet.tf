# Terraform Script to build infrastructure needed for Azure Demos
# Run scripts from Azure portal CLI
# You MUST create the connect object to connect the VNG to the VNET after this script is run
# otherwise you will not learn routes from the Vnet even though peering will be established

provider "azurerm" {
  version = "~>2.0"
  features {}
}

resource "azurerm_resource_group" "peering-demo-rg" {
  name = "peering-demo-rg"
  location = "North Central US"
}

resource "azurerm_express_route_circuit" "expressroutedemo" {
  name                  = "ExpressRouteDemo"
  resource_group_name   = azurerm_resource_group.peering-demo-rg.name
  location              = azurerm_resource_group.peering-demo-rg.location
  service_provider_name = "Equinix"
  peering_location      = "Chicago"
  bandwidth_in_mbps     = 50

  sku {
    tier   = "Standard"
    family = "MeteredData"
  }

  tags = {
    environment = "Demo"
  }
}

output "service_key" {
    value       = azurerm_express_route_circuit.expressroutedemo.service_key
 }

 resource "azurerm_express_route_circuit_peering" "er_peering" {
  peering_type                  = "AzurePrivatePeering"
  express_route_circuit_name    = azurerm_express_route_circuit.expressroutedemo.name
  resource_group_name           = azurerm_resource_group.peering-demo-rg.name
  peer_asn                      = 65000
  primary_peer_address_prefix   = "10.0.0.4/30"
  secondary_peer_address_prefix = "10.0.0.8/30"
  vlan_id                       = 100

}

resource "azurerm_virtual_network" "vng-vnet" {
  name                = "vnet"
  location            = azurerm_resource_group.peering-demo-rg.location
  resource_group_name = azurerm_resource_group.peering-demo-rg.name
  address_space       = ["10.20.0.0/16"]
  dns_servers         = ["10.20.0.4", "10.20.0.5"]

    subnet {
    name           = "subnet1"
    address_prefix = "10.20.1.0/24"
  }

  subnet {
    name           = "subnet2"
    address_prefix = "10.20.2.0/24"
  }

  subnet {
    name           = "subnet3"
    address_prefix = "10.20.3.0/24"
     }

  tags = {
    environment = "Demo"
  }
}

resource "azurerm_subnet" "vng-private" {
  name                 = "GatewaySubnet"
  resource_group_name  = azurerm_resource_group.peering-demo-rg.name
  virtual_network_name = azurerm_virtual_network.vng-vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_public_ip" "public_ip" {
  name                = "PublicIP"
  resource_group_name = azurerm_resource_group.peering-demo-rg.name
  location            = azurerm_resource_group.peering-demo-rg.location
  allocation_method   = "Dynamic"

}

resource "azurerm_virtual_network_gateway" "vng" {
  name                = "vng"
  location            = azurerm_resource_group.peering-demo-rg.location
  resource_group_name = azurerm_resource_group.peering-demo-rg.name

  type                = "ExpressRoute"
  enable_bgp          = true
  sku                 = "Standard"

  ip_configuration {
    name                          = "vnetGWIPAddressing"
    public_ip_address_id          = azurerm_public_ip.public_ip.id
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.vng-private.id
  }
}

